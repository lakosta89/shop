/**
 * Created by MKA on 10.07.2020.
 */

global class Select150Accounts {
@future (Callout = true)
public static void SeachAccount (List<Id> listId){
    List<Account> acct = [SELECT Id, Name
                            FROM Account
                            WHERE Id IN: listId
                            LIMIT 150];
}
}