@isTest
public class TestOpportunityChangeTrigger {
    @isTest static void testCreateAndUpdateOpportunity(){
        Test.enableChangeDataCapture();
        Opportunity opp = new Opportunity();
        opp.Name = 'Sell 100 Widgets';
        opp.StageName = 'Prospecting';
        opp.CloseDate = Date.today().addMonths(3);
        
         Test.getEventBus().deliver();
        
        Task[] taskList = [SELECT Id,Subject FROM Task]; 
        System.assertEquals(0, taskList.size(),
                            'Unexpected task found');
Opportunity oppt = [SELECT Id, Name, StageName, isWon, CloseDate from Opportunity][0];    // Debug
    //System.debug('Retrieved employee record: ' + oppt);
    // Update one field and empty anothe
    oppt.StageName = 'Closed Won';
    update oppt;
        
        Test.getEventBus().deliver();
        taskList = [SELECT Id,Subject FROM Task];
    System.assertEquals(1, taskList.size(),
      'The change event trigger did not create the expected task.');
    }
   
}